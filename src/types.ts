export const TYPES = {
	App: Symbol.for('App'),
	UserController: Symbol.for('UserController'),
	AuthController: Symbol.for('AuthController'),
	UserService: Symbol.for('UserService'),
	UserModel: Symbol.for('UserModel'),
	TokenServise: Symbol.for('TokenServise'),
};
